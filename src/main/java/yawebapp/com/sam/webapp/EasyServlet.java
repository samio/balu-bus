package yawebapp.com.sam.webapp;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.*;
import javax.servlet.http.*;

public class EasyServlet extends HttpServlet {
	Map<String, Function<String[], String>> methodHandles = new HashMap<>();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doRegisterMethods("add", (String[] nums) -> this.add(nums[0], nums[1]));
		String resp = doProcessRequest(request);
		PrintWriter out = response.getWriter();
		out.println(resp);
		out.flush();
		out.close();
		// request.getRequestDispatcher("/simple.jsp").forward(request,
		// response);
		// doProcessRequest(request);

	}

	private String doProcessRequest(HttpServletRequest request) {
		String service = request.getHeader("serviceName");
		String first = request.getHeader("first");
		String second = request.getHeader("second");
		String[] strs = new String[2];
		strs[0] = first;
		strs[1] = second;
		String resp = null;

		if (methodHandles.containsKey(service)) {
			Function<String[], String> fn = methodHandles.get(service);
			resp = fn.apply(strs);
		}
		return resp;
	}

	public String add(String a, String b) {
		String c = String.join("-", a, b);
		System.out.println(c);
		return c;
	}

	public void doRegisterMethods(String methodName, Function<String[], String> fn) {
		methodHandles = new HashMap<>();
		if (!methodHandles.containsKey(methodName)) {
			methodHandles.put(methodName, fn);
		}
	}
}