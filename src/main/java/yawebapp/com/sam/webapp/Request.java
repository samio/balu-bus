package yawebapp.com.sam.webapp;

import java.util.Map;

public interface Request {
	public String getServiceName();

	public void setServiceName(String serviceName);

	public Map<String, String> getParams();

	public void setParams(Map<String, String> params);
}
