package yawebapp.com.sam.webapp;

import java.util.HashMap;
import java.util.Map;

public class RequestImpl {
	public String serviceName;
	public Map<String,String> params=new HashMap<String,String>();
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Map<String,String> getParams() {
		return params;
	}
	public void setParams(Map<String,String> params) {
		this.params = params;
	}
}
