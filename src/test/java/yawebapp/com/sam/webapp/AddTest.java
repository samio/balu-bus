package yawebapp.com.sam.webapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Test;

public class AddTest {
	public static String connString = "http://localhost:8080/com.sam.webapp/easy";

	public String add(String i, String j) throws ClientProtocolException, IOException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(connString);
		String line = "";
		// add request header
		request.addHeader("serviceName", "add");
		request.addHeader("first", i);
		request.addHeader("second", j);
		HttpResponse response = client.execute(request);

		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String result = "";
		while ((line = rd.readLine()) != null) {
			result += line;
		}
		return result;
	}

	@Test
	public void test1() throws ClientProtocolException, IOException {
		String first = "samio";
		String second = "sam";
		Map<String,String> params=new HashMap<String,String>();
		params.put("first", "samio");
		params.put("second", "is not a romio");
		RequestImpl req=new RequestImpl();
		req.setServiceName("add");
		req.setParams(params);
		String res = add(first, second);
		Assert.assertEquals(String.join("-", first, second), res);
	}
}
